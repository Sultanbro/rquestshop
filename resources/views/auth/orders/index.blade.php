@extends('auth.layouts.master')

@section('title', 'Заказы')

@section('content')
    <div class="col-md-12">
        <h1>@if(!Auth::user()->is_admin) Заказы @else Мои заказы @endif</h1>
        <table class="table">
            <tbody>
            <tr>
                <th>
                    #
                </th>
                @if(!Auth::user()->is_admin)
                    <th>
                        Заказчик
                    </th>
                @endif
                <th>
                    Производитель
                </th>
                <th>
                    Наименование запчасти
                </th>
                <th>
                    Сумма
                </th>
            </tr>
            @foreach($orders as $order)
                <tr>
                    <td>{{ $order->id }}</td>
                    @if(!Auth::user()->is_admin)
                    <td>{{ $order->u_name->name }}</td>
                    @endif
                    <td>{{ $order->m_name->name }}</td>
                    <td>{{ $order->p_name->name }}</td>
                    <td>{{ $order->start_price. '-' .$order->end_price }}</td>
                    <td>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
        @if(Auth::user()->is_admin)
        <a class="btn btn-success" type="button"
           href="{{ route('order_create') }}">Добавить заказ</a>
        @endif
    </div>
@endsection
