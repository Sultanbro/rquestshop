@extends('auth.layouts.master')

    @section('title', 'Создать заказ')

@section('content')
    <div class="col-md-12">

            <h1>Добавить Категорию</h1>

        <form method="POST" enctype="multipart/form-data"

              action="{{ route('order_store') }}"
        >
            <div>

                @csrf
                <div class="input-group row">
                    <label for="category_id" class="col-sm-2 col-form-label">Производитель: </label>
                    <div class="col-sm-6">
                        <select name="manufacturer" id="manufacturer" class="form-control">
                            @foreach($models as $model)
                                <option value="{{ $model->id }}"
                                        selected
                                >{{ $model->name }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <br>
                <div class="input-group row">
                    <label for="category_id" class="col-sm-2 col-form-label">Наименование запчасти: </label>
                    <div class="col-sm-6">
                        <select name="part_name" id="part_name" class="form-control">
                            @foreach($products as $product)
                                <option value="{{ $product->id }}"
                                        selected
                                >{{ $product->name }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <br>
                <div class="input-group row">
                    <label for="description" class="col-sm-2 col-form-label">Сумма от: </label>
                    <div class="col-sm-6">
                        @error('start_price')
                        <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                        <input type="number" class="form-control" name="start_price" id="start_price"
                               value="">
                    </div>
                </div>
                <br>

                <div class="input-group row">
                    <label for="description" class="col-sm-2 col-form-label">Сумма до: </label>
                    <div class="col-sm-6">
                        @error('end_price')
                        <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                        <input type="number" class="form-control" name="end_price" id="end_price"
                               value="">
                    </div>
                </div>
                <br>

                <div class="input-group row">
                    <label for="image" class="col-sm-2 col-form-label">Картинка: </label>
                    <div class="col-sm-10">
                        <label class="btn btn-default btn-file">
                            Загрузить <input type="file" style="display: none;" name="image" id="image">
                        </label>
                    </div>
                </div>
                <button class="btn btn-success">Сохранить</button>
            </div>
        </form>
    </div>
@endsection
