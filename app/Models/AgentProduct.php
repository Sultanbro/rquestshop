<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AgentProduct extends Model
{
    use HasFactory;

    public function get_order($product)
    {
            $order = $this->whereBetween('votes', [1, 100])
                ->get();
        return $order;

    }

    public function m_name()
    {
        return $this->belongsTo(Modell::class, 'model_id');
    }

    public function p_name()
    {
        return $this->belongsTo(Product::class, 'product_id');
    }

    public function u_name()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
}
