<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Modell;

class Order extends Model
{
    use HasFactory;

    protected $fillable = [
        'manufacturer',
        'part_name',
        'start_price',
        'end_price',
        'user_id',
    ];

    public function m_name()
    {
        return $this->belongsTo(Modell::class, 'manufacturer');
    }

    public function p_name()
    {
        return $this->belongsTo(Product::class, 'part_name');
    }

    public function u_name()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

}
