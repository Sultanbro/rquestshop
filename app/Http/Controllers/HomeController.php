<?php

namespace App\Http\Controllers;

use App\Http\Requests\OrderRequest;
use App\Mail\OrderCreated;
use App\Models\AgentProduct;
use App\Models\Modell;
use App\Models\Order;
use App\Models\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;


class HomeController extends Controller
{

    public function index()
    {
        $user = Auth::user();
        $orders =[];
        if($user->is_admin) {
            $orders = Order::where('user_id', $user->id)->get();
        }else{
            $products = AgentProduct::where('user_id', $user->id)->get();
            foreach ($products as $product) {
                $orders = $this->getGoodOrder($product);
            }
        }
        return view('auth.orders.index', compact('orders'));
    }

    public function create()
    {
        $products = Product::get();
        $models = Modell::get();
        return view('auth.orders.form', compact('products', 'models'));
    }

    public function store(OrderRequest $request)
    {
        $user = Auth::user();
        $user_id = $user->id;
        $params = $request->validated();

        unset($params['image']);
        if ($request->has('image')) {
            $params['image'] = $request->file('image')->store('orders');
        }

        $products = AgentProduct::get();
        foreach ($products as $product) {
            $orders = $this->getGoodOrder($product);
            foreach ($orders as $order) {

                Mail::to($product->u_name->email)->send(new OrderCreated($order, $product));

            }
        }

        Order::updateOrCreate($params, ['user_id' => $user_id]);
        return redirect()->route('home');
    }

    public function getGoodOrder($products)
    {
//        foreach ($products as $k => $product) {
            $orders = Order::where([
                ['start_price', '<', $products->price],
                ['end_price', '>', $products->price],
                ['manufacturer', '=', $products->model_id],
                ['part_name', '=', $products->product_id],
            ])->get();

//        }
        return $orders;
    }


}
