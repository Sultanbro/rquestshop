<?php

use App\Http\Controllers\HomeController;
use App\Http\Controllers\MainController;
use App\Http\Controllers\ProductController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/logout', 'Auth\LoginController@logout')->name('get-logout');

Route::middleware(['auth'])->group(function () {
    Route::group(['middleware' => 'is_admin'], function () {
        Route::get('/order/create', [HomeController::class, 'create'])->name('order_create');
        Route::post('/order/store', [HomeController::class, 'store'])->name('order_store');
        Route::get('/', [MainController::class, 'index'])->name('index');
    });
    Route::get('/home', [HomeController::class, 'index'])->name('home');
});


